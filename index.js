// require("dotenv").config()
const express = require("express");
const app = express();
const routes = require("./routes");
const port = process.env.PORT || 8888;

app.use(
  express.urlencoded({
    extended: true,
  })
);
app.use(express.json());

app.use(routes);

app.listen(port, () => {
  console.log(`http://localhost:${PORT}`);
});
