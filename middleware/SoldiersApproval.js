const SoldiersApproval = (req, res, next) => {
  try {
    const payload = req.userLogin;
    const { SoldiersId } = req.params;

    console.log(payload, "<< Payload di Approval");

    const IdCheck = SoldiersId ? payload.SoldiersId === SoldiersId : 0;

    if (!IdCheck) {
      res.status(404).json({
        msg: "Anda bukan Usernyaa!!",
      });
      return;
    }
    next();
  } catch (error) {
    console.log(error);
  }
};

module.exports = SoldiersApproval;
