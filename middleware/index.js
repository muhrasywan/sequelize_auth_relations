const authentication = require("./authentication");
const SoldiersApproval = require("./SoldiersApproval");

module.exports = {
  authentication,
  SoldiersApproval,
};
