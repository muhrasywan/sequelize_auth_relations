const { SoldiersDivision, Soldiers, Divisions } = require("../models");

class SoldiersDivisionsController {
  static async getAllSoldiersDivision(req, res) {
    try {
      const data = await SoldiersDivision.findAll();

      res.status(200).json({ data });
    } catch (error) {
      console.log(error);
    }
  }

  static async getSoldiersDivisionById(req, res) {
    const { id } = req.params;
    const options = {
      where: {
        id,
      },
    };
    const data = await SoldiersDivision.findOne(options);
    res.status(200).json({ data });
  }

  static async createSoldiersDivision(req, res) {
    try {
      const { SoldiersId, DivisionId } = req.body;
      const payload = {
        SoldiersId,
        DivisionId,
      };
      const newSoldiersDivision = await SoldiersDivision.create(payload);
      res.status(200).json({ data: newSoldiersDivision });
    } catch (error) {
      console.log(error);
    }
  }

  static async updateSoldiersDivision(req, res) {
    const { SoldiersId, DivisionId } = req.body;

    const payload = {
      SoldiersId,
      DivisionId,
    };
    const { id } = req.params;
    const options = {
      where: {
        id,
      },
      returning: true,
    };
    const doneUpdated = await SoldiersDivision.update(payload, options);

    res.status(200).json({ data: doneUpdated });
  }

  static async deleteSoldiersDivision(req, res) {
    const { id } = req.params;
    const deleted = await SoldiersDivision.destroy({
      where: {
        id,
      },
      returning: true,
    });

    res.status(200).json({ data: deleted });
  }
}

module.exports = SoldiersDivisionsController;
