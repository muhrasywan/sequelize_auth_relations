const { Soldiers, Guns } = require("../models");
const uuid = require("uuid");
const { Token, encryption } = require("../utils");

class SoldiersController {
  static async login(req, res) {
    const { name, password } = req.body;
    const soldier = await Soldiers.findOne({
      where: {
        name,
      },
    });

    const isValid = soldier
      ? encryption.PasswordValid(password, soldier.dataValues.password)
      : 0;
    console.log(soldier);

    if (!soldier || !isValid) {
      res.status(404).json({
        status: 404,
        msg: "Anda Siapa?!!",
      });
      return;
    }

    const access_token = Token.createToken(soldier.dataValues);
    res.status(200).json({ access_token });
  }

  static async getSoldiers(req, res) {
    try {
      const options = {
        include: [
          {
            model: Guns,
            attributes: ["name", "type"],
          },
        ],
      };
      const data = await Soldiers.findAll(options);
      res.status(200).json({ data });
    } catch (error) {
      console.log(error);
    }
  }

  static async getSoldiersById(req, res) {
    const { SoldiersId } = req.params;
    const options = {
      where: {
        SoldiersId,
      },
      include: [
        {
          model: Guns,
        },
      ],
    };
    const data = await Soldiers.findOne(options);

    res.status(200).json({ data: data });
  }

  static async updateSoldiers(req, res) {
    const { name, age } = req.body;

    const payload = {
      name,
      age,
    };
    const { SoldiersId } = req.params;
    const options = {
      where: {
        SoldiersId,
      },
      returning: true,
    };
    const doneUpdate = await Soldiers.update(payload, options);

    res.status(200).json({ data: doneUpdate });
  }

  static async createSoldiers(req, res) {
    const SoldiersId = uuid.v4();
    let { name, age, password } = req.body;
    password = encryption.encryptPassword(password);
    const payload = {
      name,
      age,
      SoldiersId,
      password,
    };
    console.log(password, "<<< password di Create");

    const createdSoldiers = await Soldiers.create(payload);
    console.log(createdSoldiers);
    res.status(200).json({ data: createdSoldiers });
  }

  static async deleteSoldiers(req, res) {
    const { SoldiersId } = req.params;
    const deleteSoldiers = await Soldiers.destroy({
      where: {
        SoldiersId,
      },
      returning: true,
    });

    res.status(200).json({ data: deleteSoldiers });
  }
}

module.exports = SoldiersController;
