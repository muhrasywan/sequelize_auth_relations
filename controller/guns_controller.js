const { Guns, Soldiers } = require("../models");
const uuid = require("uuid");

class GunsController {
  static async getGuns(req, res) {
    try {
      const options = {
        include: [
          {
            model: Soldiers,
            attributes: ["name"],
          },
        ],
      };
      const data = await Guns.findAll(options);
      res.status(200).json({ data: data });
    } catch (error) {
      console.log(error);
    }
  }

  static async getGunsById(req, res) {
    const { GunsId } = req.params;
    const options = {
      where: {
        GunsId,
      },
    };
    const data = await Guns.findOne(options);

    res.status(200).json({ data: data });
  }

  static async updateGuns(req, res) {
    const { name, type, SoldierSoldiersId } = req.body;

    const payload = {
      name,
      type,
      SoldierSoldiersId,
    };
    const { GunsId } = req.params;
    const options = {
      where: {
        GunsId,
      },
      returning: true,
    };
    const updateGuns = await Guns.update(payload, options);

    res.status(200).json({ data: updateGuns });
  }

  static async createGuns(req, res) {
    const GunsId = uuid.v4();
    const { name, type, SoldierSoldiersId } = req.body;
    const payload = {
      name,
      type,
      SoldierSoldiersId,
      GunsId,
    };

    const createGuns = await Guns.create(payload);
    res.status(200).json({ data: createGuns });
  }

  static async deleteGuns(req, res) {
    const { GunsId } = req.params;
    const deleteGuns = await Guns.destroy({
      where: {
        GunsId,
      },
      returning: true,
    });

    res.status(200).json({ data: deleteGuns });
  }
}

module.exports = GunsController;
