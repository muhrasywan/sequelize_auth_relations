const { Soldiers, Divisions } = require("../models");

class DivisionsController {
  static async getDivisions(req, res) {
    try {
      const options = {
        include: [
          {
            model: Soldiers,
          },
        ],
      };
      const data = await Divisions.findAll(options);
      res.status(200).json({ data });
    } catch (error) {
      console.log(error);
    }
  }

  static async getDivisionById(req, res) {
    const { id } = req.params;
    const options = {
      where: {
        id,
      },
    };
    const data = await Divisions.findOne(options);
    res.status(200).json({ data });
  }

  static async updateDivisions(req, res) {
    const { name, location } = req.body;

    const payload = {
      name,
      location,
    };
    const { id } = req.params;
    const options = {
      where: {
        id,
      },
      returning: true,
    };
    const doneUpdated = await Divisions.update(payload, options);
    res.status(200).json({ data: doneUpdated });
  }

  static async createDivisions(req, res) {
    const { name, location } = req.body;
    const payload = {
      name,
      location,
    };
    const createDivisions = await Divisions.create(payload);
    res.status(200).json({ data: createDivisions });
  }

  static async deleteDivisions(req, res) {
    const { id } = req.params;
    const deleted = await Divisions.destroy({
      where: {
        id,
      },
      returning: true,
    });
    res.status(200).json({ data: deleted });
  }
}

module.exports = DivisionsController;
