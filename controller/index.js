const SoldiersController = require("./soldiers_controller");
const GunsController = require("./guns_controller");
const DivisionController = require("./divisions_controller");
const SoldiersDivisionsController = require("./soldiersDivision_controller");

module.exports = {
  SoldiersController,
  GunsController,
  DivisionController,
  SoldiersDivisionsController,
};
