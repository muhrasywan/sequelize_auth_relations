const bcrypt = require("bcryptjs")

class Encryption{
    static encryptPassword(raw){
        try {
            const salt = bcrypt.genSaltSync(10)
            const hash = bcrypt.hashSync(raw, salt)
            console.log(hash,"<<< Disini Hash")
            return hash
        } catch (error){
            return null
        }
    }

    static PasswordValid(raw, hash){
        try {
            return bcrypt.compareSync(raw, hash)
        } catch (error) {
            return null
        }
    }
}

module.exports = Encryption