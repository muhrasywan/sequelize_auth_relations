"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class SoldiersDivision extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.Soldiers.belongsToMany(models.Divisions, {
        through: models.SoldiersDivision,
        foreignKey: "SoldiersId",
      });
      models.Divisions.belongsToMany(models.Soldiers, {
        through: models.SoldiersDivision,
        foreignKey: "DivisionId",
      });
    }
  }
  SoldiersDivision.init(
    {
      SoldiersId: DataTypes.STRING,
      DivisionId: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "SoldiersDivision",
    }
  );
  return SoldiersDivision;
};
