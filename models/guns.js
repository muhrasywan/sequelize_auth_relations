"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Guns extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Guns.belongsTo(models.Soldiers, {
        foreignKey: "SoldierSoldiersId",
      });
    }
  }
  Guns.init(
    {
      GunsId: {
        primaryKey: true,
        type: DataTypes.STRING,
      },
      name: DataTypes.STRING,
      type: DataTypes.STRING,
      SoldierSoldiersId: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "Guns",
    }
  );
  return Guns;
};
