const { SoldiersDivisionsController } = require("../controller");
const route = require("express").Router();
const { authentication } = require("../middleware");

route.get("/", SoldiersDivisionsController.getAllSoldiersDivision);
route.get("/:id", SoldiersDivisionsController.getSoldiersDivisionById);
route.post("/", SoldiersDivisionsController.createSoldiersDivision);
route.put("/:id", SoldiersDivisionsController.updateSoldiersDivision);
route.delete("/:id", SoldiersDivisionsController.deleteSoldiersDivision);

module.exports = route;
