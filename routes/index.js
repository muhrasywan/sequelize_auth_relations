const SoldiersRoute = require("./soldiers_routes");
const GunsRoute = require("./guns_routes");
const DivisionRoute = require("./division_routes");
const SoldiersDivisionRoute = require("./soldiersDivision_routes");
const route = require("express").Router();

route.use("/soldiers", SoldiersRoute);
route.use("/guns", GunsRoute);
route.use("/divisions", DivisionRoute);
route.use("/soldiersdivision", SoldiersDivisionRoute);

module.exports = route;
