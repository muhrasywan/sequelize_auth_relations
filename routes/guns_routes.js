const { GunsController } = require("../controller");
const route = require("express").Router();

route.get("/", GunsController.getGuns);
route.get("/:GunsId", GunsController.getGunsById);
route.post("/", GunsController.createGuns);
route.put("/:GunsId", GunsController.updateGuns);
route.delete("/:GunsId", GunsController.deleteGuns);

module.exports = route;
