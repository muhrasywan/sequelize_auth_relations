const { SoldiersController } = require("../controller");
const route = require("express").Router();
const { authentication, SoldiersApproval } = require("../middleware");

route.get("/", SoldiersController.getSoldiers);
route.get("/:SoldiersId", SoldiersController.getSoldiersById);
route.post("/login", SoldiersController.login);
route.post("/", SoldiersController.createSoldiers);

route.use(authentication);

route.put("/:SoldiersId", SoldiersApproval, SoldiersController.updateSoldiers);
route.delete(
  "/:SoldiersId",
  SoldiersApproval,
  SoldiersController.deleteSoldiers
);

module.exports = route;
