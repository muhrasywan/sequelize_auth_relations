"use strict";
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("SoldiersDivisions", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      SoldiersId: {
        type: Sequelize.STRING,
        references: {
          model: "Soldiers",
          key: "SoldiersId",
        },
      },
      DivisionId: {
        type: Sequelize.INTEGER,
        references: {
          model: "Divisions",
          key: "id",
          unique: true,
        },
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("SoldiersDivisions");
  },
};
