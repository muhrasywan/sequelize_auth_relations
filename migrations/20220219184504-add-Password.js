"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    return queryInterface.addColumn("Soldiers", "password", Sequelize.STRING);
  },

  async down(queryInterface, Sequelize) {
    return queryInterface.removeColumn(
      "Soldiers",
      "password",
      Sequelize.STRING
    );
  },
};
